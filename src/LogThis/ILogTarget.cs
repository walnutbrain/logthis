﻿namespace LogThis
{
    public interface ILogTarget
    {
        bool IsActive { get; }
        void Write(LogEvent @event);
    }
}