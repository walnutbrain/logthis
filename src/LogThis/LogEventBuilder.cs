﻿using System;
using System.Collections.Generic;

namespace LogThis
{
    internal class LogEventBuilder : ILogEventBuilder
    {
        private readonly LogEvent _logEvent;

        public LogEventBuilder(LogLevel level)
        {
            _logEvent = new LogEvent(level);
        }

        public ILogEventBuilder Message(object message)
        {
            _logEvent.Message = message;
            return this;
        }

        public ILogEventBuilder Message(string format, params object[] parameters)
        {
            _logEvent.Message = string.Format(format, parameters);
            return this;
        }

        public ILogEventBuilder Exception(Exception exception)
        {
            _logEvent.Exception = exception;
            return this;
        }

        public ILogEventBuilder AddToSlot(string slotName, object value)
        {
            var slot = _logEvent[slotName];
            if (slot == null)
                _logEvent[slotName] = value;
            else
            {
                var list = slot as SlotList;
                if (list != null)
                    list.Add(value);
                else
                {
                    list = new SlotList {slot, value};
                    _logEvent[slotName] = list;
                }

            }
            return this;
        }

        internal LogEvent Build()
        {
            return _logEvent;
        }
    }

    internal class SlotList : List<object>
    {
        
    }
}