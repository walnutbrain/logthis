﻿using System;
using System.Collections.Generic;

namespace LogThis
{
    public class LogEvent
    {
        public LogLevel Level { get; private set; }

        internal LogEvent(LogLevel level)
        {
            Level = level;
        }

        private readonly Dictionary<string, object> _slots = new Dictionary<string, object>(); 
        public object Message { get; internal set; }
        public Exception Exception { get; internal set; }


        public IEnumerable<string> SlotNames => _slots.Keys;

        public object this[string name]
        {
            get
            {
                object result;
                _slots.TryGetValue(name, out result);
                return result;
            }
            internal set { _slots[name] = value; }
        }
    }
}