using System;

namespace LogThis
{
    public interface ILoggingProvider
    {
        ILogTarget GetTargetForObject(LogLevel level, object source);
        ILogTarget GetTargetForType(LogLevel level, Type source);
        ILogTarget GetTargetForString(LogLevel level, string source);
        ILogTarget GetTargetForNull(LogLevel level);
    }
}