﻿using System;

namespace LogThis
{
    public interface ILogEventBuilder
    {
        ILogEventBuilder Message(object message);
        ILogEventBuilder Message(string format, params object[] parameters);
        ILogEventBuilder Exception(Exception exception);
        ILogEventBuilder AddToSlot(string slotName, object value);
    }
}