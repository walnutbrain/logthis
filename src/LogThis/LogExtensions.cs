﻿using System;

namespace LogThis
{
    public static class LogExtensions
    {
        private static ILoggingProvider Provider { get; set; }

        private static void Log(object source, LogLevel level, Action<ILogEventBuilder> log)
        {
            if(Provider == null) return;
            ILogTarget target;
            if (source == null)
                target = Provider.GetTargetForNull(level);
            else if (source.GetType() == typeof (Type))
                target = Provider.GetTargetForType(level, (Type) source);
            else if (source is string)
                target = Provider.GetTargetForString(level, (string) source);
            else
                target = Provider.GetTargetForObject(level, source);
            if (!target.IsActive) return;
            var builder = new LogEventBuilder(level);
            log(builder);
            target.Write(builder.Build());
        }

        public static void FatalLog(this object source, Action<ILogEventBuilder> log)
        {
            Log(source, LogLevel.Fatal, log);
        }

        public static void ErrorLog(this object source, Action<ILogEventBuilder> log)
        {
            Log(source, LogLevel.Error, log);
        }

        public static void WarnLog(this object source, Action<ILogEventBuilder> log)
        {
            Log(source, LogLevel.Warn, log);
        }

        public static void InfoLog(this object source, Action<ILogEventBuilder> log)
        {
            Log(source, LogLevel.Info, log);
        }

        public static void DebugLog(this object source, Action<ILogEventBuilder> log)
        {
            Log(source, LogLevel.Debug, log);
        }

        public static void TraceLog(this object source, Action<ILogEventBuilder> log)
        {
            Log(source, LogLevel.Trace, log);
        }

        public static void SetProvider(ILoggingProvider provider)
        {
            Provider = provider;
        }
    }
}