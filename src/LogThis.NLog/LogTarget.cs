using System;
using Newtonsoft.Json;
using NLog;
using NLog.Fluent;

namespace LogThis.NLog
{
    internal class LogTarget : ILogTarget
    {
        private readonly LogBuilder _builder;

        private static global::NLog.LogLevel ConvertLevel(LogLevel level)
        {
            switch (level)
            {
                case LogLevel.Fatal:
                    return global::NLog.LogLevel.Fatal;
                case LogLevel.Error:
                    return global::NLog.LogLevel.Error;
                case LogLevel.Debug:
                    return global::NLog.LogLevel.Debug;
                case LogLevel.Info:
                    return global::NLog.LogLevel.Info;
                case LogLevel.Trace:
                    return global::NLog.LogLevel.Trace;
                case LogLevel.Warn:
                    return global::NLog.LogLevel.Warn;
                default:
                    throw new ArgumentOutOfRangeException($"Unknown LogLevel {level}");
            }
        }

        public LogTarget(LogLevel level, Logger logger)
        {
            IsActive = logger.IsEnabled(ConvertLevel(level));
            _builder = logger.Log(ConvertLevel(level));
        }

        public bool IsActive { get; }

        public void Write(LogEvent @event)
        {
            _builder
                .Message(JsonConvert.SerializeObject(@event.Message))
                .Exception(@event.Exception);
            foreach (var name in @event.SlotNames)
            {
                _builder.Property(name, JsonConvert.SerializeObject(@event[name]));
            }
            _builder.Write();
        }
    }
}