﻿using System;
using NLog;

namespace LogThis.NLog
{
    internal class LoggingProvider : ILoggingProvider
    {
        public ILogTarget GetTargetForObject(LogLevel level, object source)
        {
            return new LogTarget(level, LogManager.GetLogger(source.GetType().FullName));
        }

        public ILogTarget GetTargetForType(LogLevel level, Type source)
        {
            return new LogTarget(level, LogManager.GetLogger(source.FullName));
        }

        public ILogTarget GetTargetForString(LogLevel level, string source)
        {
            return new LogTarget(level, LogManager.GetLogger(source));
        }

        public ILogTarget GetTargetForNull(LogLevel level)
        {
            return new LogTarget(level, LogManager.GetLogger(""));
        }
    }
}