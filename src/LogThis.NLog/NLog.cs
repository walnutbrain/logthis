﻿namespace LogThis.NLog
{
    public static class NLog
    {
        public static void Use()
        {
            LogExtensions.SetProvider(new LoggingProvider());
        }
    }
}